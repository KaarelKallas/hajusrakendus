<?php

namespace App\Http\Controllers;

use App\Models\FavoriteGames;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class FavoriteGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        return Inertia::render('FavoriteGames/Index', [
            'games' => FavoriteGames::all(),
            'images' => Media::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:500',
            'playtime' => 'required|string|max:255',
            'rating' => 'required|string|max:255',
        ]);
       $fgames = FavoriteGames::create($validated);
        if ($images = $request->file('images')) {
            foreach ($images as $image) {
                $fgames->addMedia($image)->toMediaCollection('images');
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(FavoriteGames $favoriteGames)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FavoriteGames $favoriteGames)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FavoriteGames $favoriteGames)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FavoriteGames $favoriteGames)
    {
        //
    }
}
